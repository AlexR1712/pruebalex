interface TagsInterface {
  string;
}

interface FriendsInterface {
  id: number;
  name: string;
}

export interface ClientInterface {
  id: number;
  guid: string;
  isActive: boolean;
  balance: string;
  picture: string;
  age: number;
  eyeColor: string;
  name: string;
  gender: string;
  company: string;
  email: string;
  phone: string;
  address: string;
  about: string;
  registered: Date;
  latitude: number;
  longitude: number;
  tags: Array<TagsInterface>;
  friends: Array<FriendsInterface>;
  greeting: string;
  favoriteFruit: string;
}
