import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientDetailsComponent } from './components/client-details/client-details.component';
import { HomeComponent } from './components/home/home.component';



const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'client/:id', component: ClientDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
