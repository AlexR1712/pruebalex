import { Component, OnInit } from '@angular/core';
import { ClientDataService } from '../../services/client-data.service';
import { ClientInterface } from '../../models/client-interface';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  clients: ClientInterface[];
  constructor(private clientData: ClientDataService, private route: ActivatedRoute, private router: Router
    ) { }

  ngOnInit(): void {
    this.getClients();
  }

  async getClients() {
    const data = await this.clientData.getAllClients();
    this.clients  = data;
  }

  goTo(id) {
    console.log(id);
    this.router.navigate([`client/${id}`], { state:  this.clients[id]});
  }

}
