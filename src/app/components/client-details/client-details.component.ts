import { Component, OnInit } from '@angular/core';
import { ClientDataService } from '../../services/client-data.service';
import { ClientInterface } from '../../models/client-interface';
import { ActivatedRoute, Router, NavigationStart } from '@angular/router';


@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.scss']
})
export class ClientDetailsComponent implements OnInit {
    id: number;
    client: ClientInterface;
    clients: ClientInterface[];
    loading = true;
    active = 1;
    innerWidth: any;
    isCollapsed = true;
  constructor(
    private clientData: ClientDataService,
    private route: ActivatedRoute,
    private router: Router
  ) {}


  async getClients() {
    const data = await this.clientData.getAllClients();
    this.clients = data;
  }

  async ngOnInit(): Promise<any> {
    this.innerWidth = window.innerWidth;
    this.isCollapsed = this.innerWidth <= 992 ? true : false;
    console.log(this.innerWidth);
    if (!history.state.guid) {
      this.id = Number(this.route.snapshot.paramMap.get('id'));
      await this.getClients();
      this.client = this.clients[this.id];
      this.loading = false;
    } else {
      this.client = history.state;
      this.loading = false;
    }
  }


  onResize() {
    this.innerWidth = window.innerWidth;
    this.isCollapsed = this.innerWidth <= 992 ? true : false;
  }


}




