import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ClientInterface } from '../models/client-interface';



@Injectable({
  providedIn: 'root'
})
export class ClientDataService {

  // tslint:disable-next-line:max-line-length
  url = 'https://gist.githubusercontent.com/AlexR1712/86ca3e58994a3ca4023eb6123eb76763/raw/f93953a95b69b02f2d7ab3e64cbbf0cc976f0f9e/db.json';
  // url = './assets/db.json';

  constructor(private http: HttpClient) { }


  async getAllClients() {
    const data = await this.http.get<ClientInterface[]>(this.url).toPromise();
    return data;

  }
}
